var Handlebars = require('handlebars');

module.exports = {
    defaultLayout: 'main',
    helpers: {
        section: function(name, options){
            if(!this._sections) this._sections = {};
            this._sections[name] = options.fn(this);
            return null;
        },
        toLowerString: function(str, options) {
            return str.toString().toLowerCase();
        },
        everyOther: function(index, amount, scope) {
            if ( ++index % amount)
                return scope.inverse(this);
            else
                return scope.fn(this);
        },
        json: function(context) {
            return JSON.stringify(context);
        },
        greaterThan: function(context, options) {
            if (context > options) {
                return true;
            } else {
                return false;
            }
        }
    }
};